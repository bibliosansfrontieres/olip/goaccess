# Goaccess

Goaccess allows to build visual reports.

Reports are available at <http://stats.ideascube.io>.
The default user/password is `admin`/`admin`.

You can check the [user documentation](https://gitlab.com/bibliosansfrontieres/olip/olip-user-documentation/-/wikis/en/Statistics)
for log explanation.

**4 differents reports are available :**

* Cumulative report
* Daily report
* Weekly report
* Monthly report

The links to each report are available from the homepage.

**Note:** The source files of the reports are available in the hosts's folder `/data/logs_reports/`.

## Reports: Automatic build

The cumulative report is updated every 10 minutes.
A cron job that builds the 3 other reports is launched every day at 12:05 AM.

## Reports: Manual build

To build manually the cumulative report, launch

```shell
balena-engine exec stats /report_builder cumulative
```

To build manually the daily report, launch

```shell
balena-engine exec stats /report_builder daily
```

To build manually the weekly report, launch

```shell
balena-engine exec stats /report_builder weekly
```

To build manually the monthly report, launch

```shell
balena-engine exec stats /report_builder monthly
```

## Important notes

The daily, weekly and monthly reports are only generated if a line of log
exists within the requested timespan
(respectively: 1 day, 7 days, 30 days from the current date).

## Aggregating logs

Goaccess is set to build reports from all the `access.log*` files under the folder
`logs/`. Thus to aggregate several logs in a single reports, one's need to
copy/paste the log files in that folder and run the commands stated above
(or wait for the cron to launch).

## Share the reports

Each html report is a standalone report page thus can be downloaded and sent
to other users (that have no access to the current site).

## This image

* Source: <https://gitlab.com/bibliosansfrontieres/olip/goaccess>
* Docker Hub: [`offlineinternet/goaccess`](https://hub.docker.com/repository/docker/offlineinternetgoaccess)
* GitLab Registry: [`registry.gitlab.com/bibliosansfrontieres/olip/goaccess/goaccess`](https://gitlab.com/bibliosansfrontieres/olip/goaccess/container_registry/2302192)
