# hadolint ignore=DL3007
FROM offlineinternet/olip-base:latest

# hadolint ignore=DL3008
RUN apt-get --quiet --quiet update \
    && apt-get install --no-install-recommends --quiet --quiet --yes \
      cron \
      geoip-database \
      nginx \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    \
    && rm -f /var/www/html/index.nginx-debian.html

# https://goaccess.io/download#official-repo
# /!\ release name hardcoded to bionic /!\
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
# hadolint ignore=DL3008
RUN \
  apt-get update --quiet --quiet \
  && apt-get install --no-install-recommends --quiet --quiet --yes \
    gnupg \
  && \
  wget --no-verbose -O- https://deb.goaccess.io/gnugpg.key \
    | gpg --dearmor \
    | tee /usr/share/keyrings/goaccess.gpg >/dev/null \
  && echo "deb [signed-by=/usr/share/keyrings/goaccess.gpg] http://deb.goaccess.io/ bionic main" \
    | tee /etc/apt/sources.list.d/goaccess.list \
  && apt-get update --quiet --quiet \
  && apt-get install --no-install-recommends --quiet --quiet --yes \
    libncursesw5-dev \
    goaccess \
  && apt-get purge --autoremove gnupg --yes \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
  \
  && rm -rf \
    /usr/share/doc/goaccess \
    /usr/share/man/man1/goaccess.1.gz \
    /etc/goaccess/goaccess.conf \
  \
  && echo && echo "[+] Version check:" \
  && goaccess --version \
  && echo

# GoAccess configuration
COPY ./goaccess.conf /etc/goaccess/goaccess.conf

# Reporting script
COPY ./report_builder /
COPY ./report /root/report
RUN chmod +x /report_builder

# nginx conf
COPY ./nginx.conf /etc/nginx/sites-available/default


COPY ./entrypoint.sh /
RUN chmod +x /entrypoint.sh

EXPOSE 80
WORKDIR /var/www/html
ENTRYPOINT ["/entrypoint.sh"]
