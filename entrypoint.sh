#!/bin/bash

say() {
    >&2 echo "$( date '+%F %T') Docker: $*"
}

say "GoAccess container is starting."

goaccess --version | while read -r line ; do say "  $line" ; done

# Move goaccess related file to working dir
cp -rf /root/report/. /var/www/html/

say "Starting nginx..."
service nginx start

say "Installing cronjob..."
echo "*/10 * * * *    /report_builder cumulative      >/proc/1/fd/1    2>/proc/1/fd/2
5    0 * * *    /report_builder daily           >/proc/1/fd/1    2>/proc/1/fd/2
10   0 * * *    /report_builder weekly          >/proc/1/fd/1    2>/proc/1/fd/2
15   0 * * *    /report_builder monthly         >/proc/1/fd/1    2>/proc/1/fd/2
" | crontab # Don't remove the empty line at the end of this string. It is required to run the cron job.

say "Container is ready!"

# Dump configuration
say "Runtime crontab:"
crontab -l | while read -r line ; do say "  $line" ; done
logfiles_count="$( find /logs/ -type f -name "access.log*" 2>/dev/null | wc -l )"
if (( logfiles_count > 0 )); then
    say "$logfiles_count logfiles found."
    if [ ! -r /var/www/html/index.html ]; then
        say "No report found, generating a first report to avoid 403 errors..."
        /report_builder cumulative
    fi
else
    say "No log files found - yet."
fi

say "Starting cron daemon..."
cron -f
