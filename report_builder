#!/bin/bash

# The current files needs the following params
TITLE='OLIP dashboard'

CUSTOM_CSS='static/report.css'
CUSTOM_JS='static/report.js'

say() {
    >&2 echo "$( date '+%F %T') GoAccess: $*"
}

logfiles_count="$( find /logs/ -type f -name "access.log*" 2>/dev/null | wc -l )" 
(( logfiles_count > 0 )) || {
  say "No log files found - yet."
  exit 0
}

# call goaccess with common switches
run_goaccess() {
  goaccess - \
    --config-file=/etc/goaccess/goaccess.conf \
    --html-report-title="$TITLE" \
    --html-custom-css="$CUSTOM_CSS" \
    --html-custom-js="$CUSTOM_JS" \
    --ignore-panel=GEO_LOCATION --ignore-panel=REFERRING_SITES \
    --ignore-panel=REQUESTS_STATIC --ignore-panel=OS --ignore-panel=REFERRERS \
    --ignore-panel=STATUS_CODES --ignore-panel=REMOTE_USER \
    --ignore-panel=BROWSERS \
    -q --no-query-string --ignore-crawlers \
    --hour-spec=min --date-spec=hr \
    -o "/var/www/html/$OUTPUT" \
    --keep-last="$DAYS"
}

# filters
filter_remove_unwanted_lines() {
  awk '!/\.js|\.css|api\@docker|api\@internal|stats|oidc|silent-renew|api-version/'
}
# "GET / HTTP/1.1" --> "GET "dashboard@internal"/ HTTP/1.1"
filter_insert_subdomain_in_target_uri() {
  awk '$7=$14$7'
}
# "GET "dashboard@internal"/ HTTP/1.1" --> "GET http://dashboard.ideascube.io/ HTTP/1.1"
filter_reformat_subdomain_in_target_uri() {
  # shellcheck disable=SC2086,SC2154
  sed 's/\"\([^ ]*\)@\([^ ]*\)\"/http:\/\/\1.'$applications_root'/'
}
# "dashboard@internal" --> "dashboard"
filter_remove_origin_from_referer() {
   sed -e 's/@docker//' -e 's/@internal//'
}
# run them all!
run_filters() {
  filter_remove_unwanted_lines \
  | filter_insert_subdomain_in_target_uri \
  | filter_reformat_subdomain_in_target_uri \
  | filter_remove_origin_from_referer
}

# cats the log files
cat_logs() {
  mtime_days=$DAYS
  [ "$DAYS" -eq 0 ] && mtime_days=3650
  find /logs/ -type f -name "access.log*" -not -name "*.gz" \
    -mtime -${mtime_days} -printf '%C@\t%p\n'\
    | sort -nk1 | cut -f2- \
    | xargs cat
  find /logs/ -type f -name "access.log*gz" \
    -mtime -${mtime_days} -printf '%C@\t%p\n'\
    | sort -nk1 | cut -f2- \
    | xargs zcat
}

# Build time based report 
build_report(){
  say "Building ${OUTPUT}..."
  # shellcheck disable=SC2086,SC2046
  cat_logs \
    | run_filters \
    | run_goaccess
}  

# Usage
case "$1" in 
  'daily') 
    OUTPUT=report_day.html 
    DAYS=1
    ;;
  'weekly') 
    OUTPUT=report_week.html 
    DAYS=7
    ;;      
  'monthly')
    OUTPUT=report_month.html
    DAYS=30
    ;;
  'cumulative')
    OUTPUT=index.html
    DAYS=0
    ;;
  *)
    echo "Usage: report_builder {daily|weekly|monthly|cumulative}"
    exit 1
    ;;
esac

build_report
